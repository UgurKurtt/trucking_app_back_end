import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { Driver } from './model/driver';
import { DRIVERS } from './model/mock-drivers';

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  getDriver(id: string):Observable<Driver> {
    return of(DRIVERS.find(driver => driver.__id == id));
  }

  constructor(private messageService: MessageService) { }

  getDrivers(): Observable<Driver[]> {
    this.messageService.add('DriverService: fetched drivers');
    return of(DRIVERS);
  }
}
