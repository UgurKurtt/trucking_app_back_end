import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../model/vehicle';
import { MessageService } from '../message.service';
import { VehicleService } from '../vehicle.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  vehicles: Vehicle[];

  constructor(private vehicleService: VehicleService, private messageService: MessageService) { }

  getDrivers(): void {
    this.vehicleService.getHeroes().subscribe(vehicles => this.vehicles = vehicles);
  }

  ngOnInit(): void {
    this.getDrivers();
  }
}
