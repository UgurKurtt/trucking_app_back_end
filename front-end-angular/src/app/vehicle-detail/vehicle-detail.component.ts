import { Component, OnInit, Input} from '@angular/core';
import { Vehicle } from '../model/vehicle';
import { ActivatedRoute } from '@angular/router';
import { VehicleService } from '../vehicle.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.css']
})
export class VehicleDetailComponent implements OnInit {

  vehicle: Vehicle;

  constructor(
    private route: ActivatedRoute,
    private vehicleService: VehicleService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getVehicle();
  }

  getVehicle() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.vehicleService.getVehicle(id.toString()).subscribe(vehicle => {this.vehicle = vehicle});
  }
}
