import { Component, OnInit } from '@angular/core';
import { Driver } from '../model/driver';
import { DriverService } from '../driver.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.css']
})
export class DriversComponent implements OnInit {

  drivers: Driver[];

  constructor(private driverService: DriverService, private messageService: MessageService) { }

  getDrivers(): void {
    this.driverService.getDrivers().subscribe(drivers => this.drivers = drivers);
  }

  ngOnInit(): void {
    this.getDrivers();
  }
}
