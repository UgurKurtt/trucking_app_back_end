import { Component, OnInit, Input } from '@angular/core';
import { Driver } from '../model/driver';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { DriverService }  from '../driver.service';


@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.component.html',
  styleUrls: ['./driver-detail.component.css']
})
export class DriverDetailComponent implements OnInit {

  driver: Driver;

  constructor(
    private route: ActivatedRoute,
    private driverService: DriverService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getDriver();
  }

  getDriver(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.driverService.getDriver(id.toString())
      .subscribe(driver => this.driver = driver);
  }

}
