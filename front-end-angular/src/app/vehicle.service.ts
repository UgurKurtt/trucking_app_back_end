import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { Vehicle } from './model/vehicle';
import { Observable, of } from 'rxjs';
import { VEHICLES } from './model/mock-vehicles';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private messageService: MessageService) { }

  getVehicle(id: string): Observable<Vehicle> {
    return of(VEHICLES.find(vehicle =>vehicle.__id == id));
  }

  getHeroes(): Observable<Vehicle[]> {
    this.messageService.add('VehicleService: fetched vehicles');
    return of(VEHICLES);
  }
}
