export interface Vehicle {
    __id: string;
    brand: string;
    model: string;
    modelYear: Number;
    boughtValue: Number;
    type: string;
    plateNum: string;
    requiredLicences: Array<string>;
}