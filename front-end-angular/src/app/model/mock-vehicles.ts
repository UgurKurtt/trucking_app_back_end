import { Vehicle } from './vehicle';

export const VEHICLES: Vehicle[] = [
    {
        __id: "501",
        brand: "Scania",
        model: "Topline",
        modelYear: 2010,
        boughtValue: 100000,
        type: "truck",
        plateNum: "43 AF 2020",
        requiredLicences: ["E"]
    },
    {
        __id: "502",
        brand: "Renault",
        model: "Magnum",
        modelYear: 2012,
        boughtValue: 120000,
        type: "truck",
        plateNum: "43 NP 505",
        requiredLicences: ["E"]
    },
    {
        __id: "503",
        brand: "Scania",
        model: "High Line",
        modelYear: 2019,
        boughtValue: 300000,
        type: "truck",
        plateNum: "43 MR 404",
        requiredLicences: ["E"]
    }
];