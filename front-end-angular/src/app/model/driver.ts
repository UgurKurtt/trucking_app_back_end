export interface Driver {
    __id: string;
    name: string;
    surname: string;
    activeInd: string;
    salary: Number;
    licences: Array<string>;
    nationalId: string;
    serviceTime: Number;
}