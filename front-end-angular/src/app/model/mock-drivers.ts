import { Driver } from './driver';

export const DRIVERS: Driver[] = [
    {
        __id: '500000',
        name: 'Ugur',
        surname: 'Kurt',
        activeInd: 'A',
        salary: 7000,
        nationalId: '15782577596',
        serviceTime: 5,
        licences: ['E']
    },
    {
        __id: '500001',
        name: 'Azize',
        surname: 'Kurt',
        activeInd: 'A',
        salary: 3000,
        nationalId: '15782571296',
        serviceTime: 26,
        licences: ['B']
    },
    {
        __id: '500002',
        name: 'Melike',
        surname: 'Kurt',
        activeInd: 'A',
        salary: 3000,
        nationalId: '15782571292',
        serviceTime: 22,
        licences: ['B']
    },
];