import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriversComponent } from './drivers/drivers.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { DriverDetailComponent } from './driver-detail/driver-detail.component';
import { VehicleDetailComponent } from './vehicle-detail/vehicle-detail.component';



const routes: Routes = [
  {path: 'drivers', component: DriversComponent},
  {path: 'vehicles', component: VehiclesComponent},
  {path: 'driver/:id', component: DriverDetailComponent},
  {path: 'vehicle/:id', component: VehicleDetailComponent},
  { path: '', redirectTo: '/drivers', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
