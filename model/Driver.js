//Require Mongoose
var mongoose = require('mongoose');
var getProperty = require('../utilities/properties.js');

var Schema = mongoose.Schema;
//Define a schema
var DriverSchema = new Schema({
  name: {type:String, required:true},
  surname: {type:String, required:true},
  activeInd: {type:String, default:getProperty('constants.activity.active')},
  birthDate: Date,
  salary: Number,
  nationalId: {type:String, required:true},
  jobStartDate: { type: Date, default: Date.now() },
  photoUrl:String,
  createTime: { type: Date, default: Date.now() },
  updateTime: { type: Date, default: Date.now() },
  licences: [ {type: Schema.Types.ObjectId, ref:'Licence'}]
});

DriverSchema.virtual('fullName').get(function () {
    return this.name + ' ' + this.surname;
  });

var DriverModel = mongoose.model('Driver', DriverSchema);

module.exports = DriverModel;