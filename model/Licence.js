//Require Mongoose
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
//Define a schema
var LicenceSchema = new Schema({
  type: String,
  remainingTime: Number,
  driver: {type: Schema.Types.ObjectId, ref:'Driver'}
});

var LicenceModel = mongoose.model('Licence', LicenceSchema);

module.exports = LicenceModel;