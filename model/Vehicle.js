//Require Mongoose
var mongoose = require('mongoose');
var getProperty = require('../utilities/properties.js');

var Schema = mongoose.Schema;
//Define a schema
var VehicleSchema = new Schema({
  brand: String,
  model: String,
  modelYear: {type:Number, min:1900, max:2100},
  boughtValue: {type:Number, min:1000, max:10000000},
  type: {type:String, enum:['truck', 'sedan', 'hatchback', 'trailer']}, //TODO: Read from properties or database
  plateNum: {type:String, required:true},
  requiredLicences: [{type:String}],
  trailers: [{type:Schema.Types.ObjectId, ref:'Vehicle'}]
});

var VehicleModel = mongoose.model('Vehicle', VehicleSchema);

module.exports = VehicleModel;