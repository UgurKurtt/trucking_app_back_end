var express = require('express');
var router = express.Router();
var Vehicle = require('../model/Vehicle.js');

router.post('/updateVehicle', function(req, res, next) {
    
    //TODO: Find a better way to update
    Vehicle.findById(req.body.vehicle._id, function(err, vehicle) {

        if(err) {console.log("Db Error");} //TODO:Catch this error and handle

        //Update driver attributes if they are filled in request
        vehicle.brand = req.body.vehicle.brand ? req.body.vehicle.brand: vehicle.brand;
        vehicle.model = req.body.vehicle.model ? req.body.vehicle.model: vehicle.model;
        vehicle.modelYear = req.body.vehicle.modelYear ? req.body.vehicle.modelYear: vehicle.modelYear;
        vehicle.boughtValue = req.body.vehicle.boughtValue ? req.body.vehicle.boughtValue: vehicle.boughtValue;
        vehicle.type = req.body.vehicle.type ? req.body.vehicle.type: vehicle.type;
        vehicle.plateNum = req.body.vehicle.plateNum ? req.body.vehicle.plateNum: vehicle.plateNum;

        //Set update time
        vehicle.updateTime = Date.now();

        vehicle.save(function(err) {
            if(err) {console.log("Db Error");} //TODO:Catch this error and handle
        });

    });

    res.send();
});


router.post('/saveVehicle', function(req, res, next) {
    
    var vehicle = new Vehicle(req.body.vehicle);

    vehicle.save(function(err) {
        if(err) {console.log('Database error');} //TODO: Catch and handle this exception
    });

    res.send();
});

router.post('/deleteVehicle', function(req, res, next) {
    
    Vehicle.findByIdAndDelete(req.body.vehicleId, function(err) {
        if(err) {console.log("Catch this exception and handle");} //TODO:Handle exception
    });

    res.send();
});

router.get('/getVehicles', function(req, res, next){
    
    Vehicle.find(req.body.vehicle, function(err, vehicles){

        if(err) {console.log('Database\'e bağlanırken hata');}

        res.send(vehicles);
    });

});

module.exports = router;