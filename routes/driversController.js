var express = require('express');
var router = express.Router();
var Driver = require('../model/Driver.js');

router.post('/updateDriver', function(req, res, next) {
    
    //TODO: Find a better way to update
    Driver.findById(req.body.driver._id, function(err, driver) {

        if(err) {console.log("Db Error");} //TODO:Catch this error and handle

        //Update driver attributes if they are filled in request
        driver.name = req.body.driver.name ? req.body.driver.name: driver.name;
        driver.surname = req.body.driver.surname ? req.body.driver.surname: driver.surname;
        driver.birthDate = req.body.driver.birthDate ? req.body.driver.birthDate: driver.birthDate;
        driver.salary = req.body.driver.salary ? req.body.driver.salary: driver.salary;
        driver.serviceTime = req.body.driver.serviceTime ? req.body.driver.serviceTime: driver.serviceTime;

        //Set update time
        driver.updateTime = Date.now();

        driver.save(function(err) {
            if(err) {console.log("Db Error");} //TODO:Catch this error and handle
        });

    });

    res.send();
});


router.post('/saveDriver', function(req, res, next) {
    
    var driver = new Driver(req.body.driver);

    driver.save(function(err) {
        if(err) {console.log('Database error');} //TODO: Catch and handle this exception
    });

    res.send();
});

router.post('/deleteDriver', function(req, res, next) {
    
    Driver.findByIdAndDelete(req.body.driverId, function(err) {
        if(err) {console.log("Catch this exception and handle");} //TODO:Handle exception
    });

    res.send();
});

router.get('/getAllDrivers', function(req, res, next){
    
    Driver.find({}, function(err, drivers){

        if(err) {console.log('Database\'e bağlanırken hata');}

        res.send(drivers);
    });

});

router.get('/getDriverByName/:name', function(req, res, next){

    Driver.find({"name": req.params.name}, function(err, drivers) {

        if(err) {console.log('Database\'e bağlanırken hata');}

        res.send(drivers);
    });

});

router.get('/getDriverById/:id', function(req, res, next){

    Driver.findById(req.params.id , function(err, drivers) {

        if(err) {console.log('Database\'e bağlanırken hata');}

        res.send(drivers);
    });

});

module.exports = router;