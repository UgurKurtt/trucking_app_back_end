var express = require('express');
var router = express.Router();
var Licence = require('../model/Licence.js');
var Driver = require('../model/Driver.js');

router.post('/updateLicence', function(req, res, next) {
    
    //TODO: Find a better way to update
    Licence.findById(req.body.licence._id, function(err, licence) {

        if(err) {console.log("Db Error");} //TODO:Catch this error and handle

        //Update driver attributes if they are filled in request
        licence.remainingTime = req.body.licence.remainingTime ? req.body.licence.remainingTime: licence.remainingTime;

        //Set update time
        licence.updateTime = Date.now();

        licence.save(function(err) {
            if(err) {console.log("Db Error");} //TODO:Catch this error and handle
        });

    });

    res.send();
});


router.post('/saveLicence', function(req, res, next) {
    
    var licence = new Licence(req.body.licence);

    licence.save(function(err) {
        if(err) {console.log('Database error');} //TODO: Catch and handle this exception
    });

    Driver.findById(req.body.driverId, function(err, driver) {
        driver.licences.push(licence._id);
        driver.save(function(err) {
            if(err) {console.log('Database error');} //TODO: Catch and handle this exception
        });
    });

    res.send();
});

router.post('/deleteLicence', function(req, res, next) {
    
    Licence.findByIdAndDelete(req.body.licenceId, function(err) {
        if(err) {console.log("Catch this exception and handle");} //TODO:Handle exception
    });

    res.send();
});

router.get('/getLicences', function(req, res, next){
    
    Licence.find(req.body.licence, function(err, licences){

        if(err) {console.log('Database\'e bağlanırken hata');}

        res.send(licences);
    });

});

module.exports = router;